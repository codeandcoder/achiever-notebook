package com.wordpress.rasanti.achievernotebook.model;

import java.util.Comparator;

public class WishComparator implements Comparator<Wish> {

	public int compare(Wish w1, Wish w2) {
		
		if ( w1.getCompletionDate() != null  && w2.getCompletionDate() != null
				&& w1.getCompletionDate().after(w2.getCompletionDate()) ) {
			return 1;
		} else if ( w1.getCompletionDate() != null  && w2.getCompletionDate() != null 
				&& w1.getCompletionDate().before(w2.getCompletionDate()) ) {
			return -1;
		} else if ( w1.getTitle().compareToIgnoreCase(w2.getTitle()) != 0 ) {
			return w1.getTitle().compareToIgnoreCase(w2.getTitle());
		} else if ( w1.getDescription().compareToIgnoreCase(w2.getDescription()) != 0 ) {
			return w1.getDescription().compareToIgnoreCase(w2.getDescription());
		}
		
		return -1;
	}

	

}
