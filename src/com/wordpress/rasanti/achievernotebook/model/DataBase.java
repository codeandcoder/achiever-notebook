package com.wordpress.rasanti.achievernotebook.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBase extends SQLiteOpenHelper{
	
	 private static final String MYDATABASE = "an_db";
	 private static final int VERSION = 2;
	 
	 public DataBase(Context connection) {
	  super(connection, MYDATABASE, null, VERSION);
	 }
	 
	 @Override
	 public void onCreate(SQLiteDatabase db) {
	  db.execSQL(TABLES.WISHES.CREATE_SQL);
	  db.execSQL(TABLES.STEPS.CREATE_SQL);
	 }
	 
	 @Override
	 public void onUpgrade(SQLiteDatabase db, int arg1, int arg2) {
	  db.execSQL(TABLES.WISHES.DESTROY_SQL);
	  db.execSQL(TABLES.STEPS.DESTROY_SQL);
	  onCreate(db);
	 }
	 
	 public static class TABLES {
		 
		 public static class STEPS {
			 
			 public static final String TABLE_NAME = "steps";
			 public static final String WISH_ID = "wish_id";
			 public static final String ID = "step_id";
			 public static final String TITLE = "step_title";
			 public static final String DESCRIPTION = "step_desc";
			 public static final String ACCOMPLISHED = "step_accomplished";
			 
			 public static final String CREATE_SQL = "CREATE TABLE IF NOT EXISTS "
					 + TABLE_NAME + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
					 + WISH_ID + " INTEGER,"
					 + TITLE + " TEXT,"
					 + ACCOMPLISHED + " INTEGER,"
					 + "FOREIGN KEY (" + WISH_ID + ") REFERENCES "
					 + WISHES.TABLE_NAME + "(" + WISHES.ID + "))";
			 
			 public static final String DESTROY_SQL = "DROP TABLE IF EXISTS " + TABLE_NAME;
			 
		 }
		 
		 public static class WISHES {
			 
			 public static final String TABLE_NAME = "wishes";
			 public static final String ID = "wish_id";
			 public static final String TITLE = "wish_title";
			 public static final String DESCRIPTION = "wish_desc";
			 public static final String INSERTION_DATE = "wish_ins_date";
			 public static final String COMPLETION_DATE = "wish_compl_date";
			 
			 public static final String CREATE_SQL = "CREATE TABLE IF NOT EXISTS "
					 + TABLE_NAME + "(" + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
					 + TITLE + " TEXT,"
					 + DESCRIPTION + " TEXT,"
					 + INSERTION_DATE + " TEXT,"
					 + COMPLETION_DATE + " TEXT)";
			 
			 public static final String DESTROY_SQL = "DROP TABLE IF EXISTS " + TABLE_NAME;
			 
		 }
		 
	 }

}
