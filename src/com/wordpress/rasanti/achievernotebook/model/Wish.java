package com.wordpress.rasanti.achievernotebook.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class Wish {

	private int id;
	private String title;
	private String description;
	private Date insertDate;
	private Date completionDate;
	
	public ArrayList<Step> getSteps() {
		return steps;
	}
	public void setSteps(ArrayList<Step> steps) {
		this.steps = steps;
	}
	private ArrayList<Step> steps;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getInsertDate() {
		return insertDate;
	}
	public String getFormattedInsertDate() {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
		return dateFormatter.format(insertDate);
	}
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getCompletionDate() {
		return completionDate;
	}
	public String getFormattedCompletionDate() {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
		return dateFormatter.format(completionDate);
	}
	public void setCompletionDate(Date completionDate) {
		this.completionDate = completionDate;
	}
	public boolean isAccomplished() {
		return completionDate != null;
	}
	
}
