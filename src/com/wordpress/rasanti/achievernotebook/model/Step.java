package com.wordpress.rasanti.achievernotebook.model;

public class Step {

	private int id;
	private int achievementId;
	private String title;
	private boolean accomplished = false;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getAchievementId() {
		return achievementId;
	}
	public void setAchievementId(int achievementId) {
		this.achievementId = achievementId;
	}
	public boolean isAccomplished() {
		return accomplished;
	}
	public void setAccomplished(boolean accomplished) {
		this.accomplished = accomplished;
	}
	
	
	
}
