package com.wordpress.rasanti.achievernotebook.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DAO {

	private static DAO dao;

	public static DAO getInstance(Context context) {
		if (dao == null)
			dao = new DAO(context);

		return dao;
	}

	private DataBase dbHelper;

	private DAO(Context context) {
		dbHelper = new DataBase(context);
	}

	public ArrayList<Step> getSteps(int wish_id) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		String[] params = new String[] { wish_id + "" };
		Cursor cur = db.rawQuery("select * from " + DataBase.TABLES.STEPS.TABLE_NAME
				+ " where " + DataBase.TABLES.STEPS.WISH_ID + " = ?", params);

		ArrayList<Step> steps = new ArrayList<Step>();

		while (cur.moveToNext()) {
			Step step = new Step();
			step.setId(cur.getInt(0));
			step.setAchievementId(cur.getInt(1));
			step.setTitle(cur.getString(2));
			step.setAccomplished(cur.getInt(3) == 1);

			steps.add(step);
		}

		db.close();
		return steps;
	}

	public boolean insertStep(Step step) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		ContentValues cv = new ContentValues();
		cv.put(DataBase.TABLES.STEPS.WISH_ID, step.getAchievementId());
		cv.put(DataBase.TABLES.STEPS.TITLE, step.getTitle());
		cv.put(DataBase.TABLES.STEPS.ACCOMPLISHED, step.isAccomplished() ? 1 : 0);

		long result = db.insert(DataBase.TABLES.STEPS.TABLE_NAME, null, cv);

		db.close();
		return result != -1;
	}

	public boolean insertWish(Wish wish) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		ContentValues cv = new ContentValues();
		cv.put(DataBase.TABLES.WISHES.TITLE, wish.getTitle());
		cv.put(DataBase.TABLES.WISHES.DESCRIPTION, wish.getDescription());
		Date currentDate = new Date();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
		cv.put(DataBase.TABLES.WISHES.INSERTION_DATE, dateFormatter.format(currentDate));
		cv.put(DataBase.TABLES.WISHES.COMPLETION_DATE, "");

		long result = db.insert(DataBase.TABLES.WISHES.TABLE_NAME, null,cv);
		
		if ( result != -1 ) {
			Wish insertedWish = getWish((int)result);
			for ( Step step : wish.getSteps() ) {
				step.setAchievementId(insertedWish.getId());
				insertStep(step);
			}
		}

		db.close();
		return result != -1;
	}
	
	public Wish getWish(int id) {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		String[] params = new String[] { id + "" };
		Cursor cur = db.rawQuery("select * from " + DataBase.TABLES.WISHES.TABLE_NAME
				+ " where " + DataBase.TABLES.WISHES.ID + " = ?", params);

		Wish wish = new Wish();
		
		if (cur.moveToNext()) {
			wish.setId(cur.getInt(0));
			wish.setTitle(cur.getString(1));
			wish.setDescription(cur.getString(2));
			wish.setInsertDate(parseDate(cur.getString(3)));
			wish.setCompletionDate(parseDate(cur.getString(4)));
			wish.setSteps(getSteps(wish.getId()));
		}

		db.close();
		return wish;
	}
	
	public ArrayList<Wish> getAllAchievedWishes() {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cur = db.rawQuery("select * from " + DataBase.TABLES.WISHES.TABLE_NAME
				+ " where " + DataBase.TABLES.WISHES.COMPLETION_DATE + " <> ''", null);

		ArrayList<Wish> wishes = new ArrayList<Wish>();

		while (cur.moveToNext()) {
			Wish wish = new Wish();
			wish.setId(cur.getInt(0));
			wish.setTitle(cur.getString(1));
			wish.setDescription(cur.getString(2));
			wish.setInsertDate(parseDate(cur.getString(3)));
			wish.setCompletionDate(parseDate(cur.getString(4)));
			wish.setSteps(getSteps(wish.getId()));

			wishes.add(wish);
		}

		db.close();
		return wishes;
	}
	
	public ArrayList<Wish> getAllUnfinishedWishes() {
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cur = db.rawQuery("select * from " + DataBase.TABLES.WISHES.TABLE_NAME + " where "
				+ DataBase.TABLES.WISHES.COMPLETION_DATE + " = ''", null);

		ArrayList<Wish> wishes = new ArrayList<Wish>();

		while (cur.moveToNext()) {
			Wish wish = new Wish();
			wish.setId(cur.getInt(0));
			wish.setTitle(cur.getString(1));
			wish.setDescription(cur.getString(2));
			wish.setInsertDate(parseDate(cur.getString(3)));
			wish.setCompletionDate(parseDate(cur.getString(4)));
			wish.setSteps(getSteps(wish.getId()));

			wishes.add(wish);
		}

		db.close();
		return wishes;
	}
	
	public void updateStep(Step step) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		ContentValues cv = new ContentValues();
		cv.put(DataBase.TABLES.STEPS.WISH_ID, step.getAchievementId());
		cv.put(DataBase.TABLES.STEPS.TITLE, step.getTitle());
		cv.put(DataBase.TABLES.STEPS.ACCOMPLISHED, step.isAccomplished() ? 1 : 0);
		
		String[] params = new String[] { step.getId()+"" };

		db.update(DataBase.TABLES.STEPS.TABLE_NAME, cv, DataBase.TABLES.STEPS.ID + " = ?", params);

		db.close();
	}
	
	public void updateWish(Wish wish) {
		SQLiteDatabase db = dbHelper.getWritableDatabase();

		ContentValues cv = new ContentValues();
		cv.put(DataBase.TABLES.WISHES.TITLE, wish.getTitle());
		cv.put(DataBase.TABLES.WISHES.DESCRIPTION, wish.getDescription());
		Date currentDate = new Date();
		SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
		cv.put(DataBase.TABLES.WISHES.INSERTION_DATE, dateFormatter.format(currentDate));
		cv.put(DataBase.TABLES.WISHES.COMPLETION_DATE, 
				"".equals(wish.getCompletionDate()) ? "" : wish.getFormattedCompletionDate());

		String[] params = new String[] { wish.getId()+"" };
		
		db.update(DataBase.TABLES.WISHES.TABLE_NAME, cv,
				DataBase.TABLES.WISHES.ID + " = ?", params);

		db.close();
	}

	private Date parseDate(String stringDate) {
		if (stringDate == null || stringDate.equals(""))
			return null;
		
		Date date = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
			date = dateFormat.parse(stringDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return date;
	}

}
