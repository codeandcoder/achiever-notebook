package com.wordpress.rasanti.achievernotebook.view;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.wordpress.rasanti.achievernotebook.R;
import com.wordpress.rasanti.achievernotebook.model.DAO;
import com.wordpress.rasanti.achievernotebook.model.Wish;
import com.wordpress.rasanti.achievernotebook.model.WishComparator;

public class MainActivity extends ActionBarActivity  implements OnClickListener, OnItemClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.main_activity);
		
		findViewById(R.id.lay_no_wish).setOnClickListener(this);
		((ListView) findViewById(R.id.list_wish)).setOnItemClickListener(this);
		((ListView) findViewById(R.id.list_last_wishes)).setOnItemClickListener(this);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		
		DAO dao = DAO.getInstance(this);
		ArrayList<Wish> unfWishes = dao.getAllUnfinishedWishes();
		
		if ( unfWishes.isEmpty() ) {
			findViewById(R.id.list_wish).setVisibility(View.GONE);
			findViewById(R.id.lay_no_wish).setVisibility(View.VISIBLE);
		} else {
			ListView lv = (ListView) findViewById(R.id.list_wish);
			lv.setVisibility(View.VISIBLE);
			findViewById(R.id.lay_no_wish).setVisibility(View.GONE);
			int randomNumber = new Random().nextInt(unfWishes.size());
			
			ArrayList<Wish> selectedWish = new ArrayList<Wish>();
			selectedWish.add(unfWishes.get(randomNumber));
			
			WishAdapter adapter = new WishAdapter(this, selectedWish);
			lv.setAdapter(adapter);
		}
		
		ArrayList<Wish> achiWishes = dao.getAllAchievedWishes();
		Collections.sort(achiWishes, new WishComparator());
		if ( !achiWishes.isEmpty() )
			achiWishes = new ArrayList<Wish>(achiWishes.subList(0, achiWishes.size() > 10 ? 10 : achiWishes.size()));
		
		if ( achiWishes.isEmpty() ) {
			findViewById(R.id.list_last_wishes).setVisibility(View.GONE);
			findViewById(R.id.tv_no_wish_acc).setVisibility(View.VISIBLE);
		} else {
			ListView lv = (ListView) findViewById(R.id.list_last_wishes);
			lv.setVisibility(View.VISIBLE);
			findViewById(R.id.tv_no_wish_acc).setVisibility(View.GONE);
			WishAdapter adapter = new WishAdapter(this, achiWishes);
			lv.setAdapter(adapter);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case R.id.wishes:
			Intent intent1 = new Intent(this, WishesActivity.class);
			startActivity(intent1);
			finish();
			break;
		case R.id.achievements:
			Intent intent2 = new Intent(this, AchievementsActivity.class);
			startActivity(intent2);
			finish();
			break;
		case R.id.settings:
			// do nothing
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClick(View v) {
		Intent intent = new Intent(this, NewWishActivity.class);
		startActivity(intent);
		finish();
	}

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		
		switch(parent.getId()) {
		case R.id.list_wish:
			Intent intent = new Intent(this, WishesActivity.class);
			startActivity(intent);
			finish();
			break;
		case R.id.list_last_wishes:
			Intent intent2 = new Intent(this, AchievementsActivity.class);
			startActivity(intent2);
			finish();
			break;
		}
		
	}

}
