package com.wordpress.rasanti.achievernotebook.view;

import java.util.ArrayList;
import java.util.Collections;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

import com.wordpress.rasanti.achievernotebook.R;
import com.wordpress.rasanti.achievernotebook.model.DAO;
import com.wordpress.rasanti.achievernotebook.model.Wish;
import com.wordpress.rasanti.achievernotebook.model.WishComparator;

public class WishesActivity extends ActionBarActivity implements
		OnClickListener, OnChildClickListener {

	private ExpandableWishAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		super.setContentView(R.layout.wishes_activity);

		findViewById(R.id.btn_new_wish).setOnClickListener(this);
		ExpandableListView elv = ((ExpandableListView) findViewById(R.id.list_wishes));
		elv.setOnChildClickListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();

		ArrayList<Wish> wishes = DAO.getInstance(this).getAllUnfinishedWishes();
		Collections.sort(wishes, new WishComparator());

		ExpandableListView elv = ((ExpandableListView) findViewById(R.id.list_wishes));
		adapter = new ExpandableWishAdapter(this, wishes);
		elv.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case R.id.wishes:
			// Do Nothing.
			break;
		case R.id.achievements:
			Intent intent2 = new Intent(this, AchievementsActivity.class);
			startActivity(intent2);
			finish();
			break;
		case R.id.settings:
			Intent intent3 = new Intent(this, MainActivity.class);
			startActivity(intent3);
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClick(View v) {
		Intent intent = new Intent(this, NewWishActivity.class);
		startActivity(intent);
		finish();
	}

	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {

		adapter.switchAccomplishmentStep(groupPosition, childPosition);
		
		return true;
	}

}
