package com.wordpress.rasanti.achievernotebook.view;

import java.util.ArrayList;

import android.app.Activity;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wordpress.rasanti.achievernotebook.R;
import com.wordpress.rasanti.achievernotebook.model.Wish;

public class WishAdapter extends BaseAdapter {

	private ArrayList<Wish> wishes;
    private LayoutInflater li;
    private Resources res;

    public WishAdapter(Activity context, ArrayList<Wish> wishes) {
            this.li = context.getLayoutInflater();
            this.res = context.getResources();
            this.wishes = wishes;
    }

    public int getCount() {
            return wishes.size();
    }

    public Object getItem(int position) {
            return wishes.get(position);
    }

    public long getItemId(int position) {
            return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder vh;
            Wish w = wishes.get(position);

            if (convertView == null) {
                    convertView = li.inflate(R.layout.wish_item, parent, false);
                    vh = new ViewHolder(convertView);
                    convertView.setTag(vh);
            } else {
                    vh = (ViewHolder) convertView.getTag();
            }
            
            vh.title.setText(w.getTitle());
            vh.description.setText(w.getDescription());
            String dates = w.getFormattedInsertDate();
            if ( w.isAccomplished() ) {
            	dates += " - " + w.getFormattedCompletionDate();
            	vh.trophy.setImageDrawable(res.getDrawable(R.drawable.trophy_mini_color));
            } else {
            	vh.trophy.setImageDrawable(res.getDrawable(R.drawable.trophy_mini));
            }
            vh.dates.setText(dates);
            return convertView;
    }
    
    private static class ViewHolder {
            
    		ImageView trophy;
            TextView title;
            TextView description;
            TextView dates;

            public ViewHolder(View view) {
            	    this.trophy = (ImageView) view.findViewById(R.id.iv_accomplished);
                    this.title = (TextView) view.findViewById(R.id.tv_title);
                    this.description = (TextView) view.findViewById(R.id.tv_description);
                    this.dates = (TextView) view.findViewById(R.id.tv_dates);
            }
    }
	
}
