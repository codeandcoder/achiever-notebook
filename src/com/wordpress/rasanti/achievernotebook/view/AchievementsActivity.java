package com.wordpress.rasanti.achievernotebook.view;

import java.util.ArrayList;
import java.util.Collections;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;

import com.wordpress.rasanti.achievernotebook.R;
import com.wordpress.rasanti.achievernotebook.model.DAO;
import com.wordpress.rasanti.achievernotebook.model.Wish;
import com.wordpress.rasanti.achievernotebook.model.WishComparator;

public class AchievementsActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.achievements_activity);
		
		ArrayList<Wish> wishes = DAO.getInstance(this).getAllAchievedWishes();
		Collections.sort(wishes, new WishComparator());
		
		ExpandableListView elv = (ExpandableListView) findViewById(R.id.list_achs);
		ExpandableWishAdapter adapter = new ExpandableWishAdapter(this, wishes);
		elv.setAdapter(adapter);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case R.id.wishes:
			Intent intent1 = new Intent(this, WishesActivity.class);
			startActivity(intent1);
			finish();
			break;
		case R.id.achievements:
			// do nothing
			break;
		case R.id.settings:
			Intent intent3 = new Intent(this, MainActivity.class);
			startActivity(intent3);
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}
}
