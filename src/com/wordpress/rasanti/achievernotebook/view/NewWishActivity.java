package com.wordpress.rasanti.achievernotebook.view;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.wordpress.rasanti.achievernotebook.R;
import com.wordpress.rasanti.achievernotebook.model.DAO;
import com.wordpress.rasanti.achievernotebook.model.Step;
import com.wordpress.rasanti.achievernotebook.model.Wish;

public class NewWishActivity extends ActionBarActivity implements OnClickListener {

	private Wish wish;
	private ArrayList<Step> steps;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_wish_activity);
		
		wish = new Wish();
		steps = new ArrayList<Step>();
		
		findViewById(R.id.btn_add_step).setOnClickListener(this);
		findViewById(R.id.btn_done).setOnClickListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case R.id.wishes:
			Intent intent1 = new Intent(this, WishesActivity.class);
			startActivity(intent1);
			finish();
			break;
		case R.id.achievements:
			Intent intent2 = new Intent(this, AchievementsActivity.class);
			startActivity(intent2);
			finish();
			break;
		case R.id.settings:
			Intent intent3 = new Intent(this, MainActivity.class);
			startActivity(intent3);
			finish();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.btn_add_step:
			addStep();
			break;
		case R.id.btn_done:
			createWish();
			break;
		case R.id.btn_remove:
			removeStep(v);
			break;
		}
	}
	
	private void removeStep(View v) {
		if ( steps.size() > 1 )
			((ViewGroup)v.getParent().getParent()).getChildAt(steps.size()-2).findViewById(R.id.btn_remove).setVisibility(View.VISIBLE);
		Step step = (Step) v.getTag();
		steps.remove(step);
		((ViewManager)v.getParent().getParent()).removeView((View)v.getParent());
	}
	
	private void addStep() {
		ViewGroup parent = (ViewGroup) findViewById(R.id.ll_new_steps);
		if ( !steps.isEmpty() )
			parent.getChildAt(parent.getChildCount()-2).findViewById(R.id.btn_remove).setVisibility(View.GONE);
		Step step = new Step();
		
		String title = ((EditText) findViewById(R.id.et_step)).getText().toString();
		if ( title == null || title.trim().equals("") ) {
			Toast.makeText(this, "Step name can't be void!", Toast.LENGTH_SHORT).show();
			return;
		}
		step.setTitle(steps.size()+1 + ". " + title);
		steps.add(step);
		
		View child = getLayoutInflater().inflate(R.layout.step_new_wish_item, parent, false);
		parent.addView(child, steps.size()-1);
		
		((TextView) child.findViewById(R.id.tv_title)).setText(step.getTitle());
		child.findViewById(R.id.btn_remove).setOnClickListener(this);
		child.findViewById(R.id.btn_remove).setTag(step);
	}
	
	private void createWish() {
		String title = ((EditText) findViewById(R.id.et_title)).getText().toString();
		if (title == null || title.trim().equals("")) {
			Toast.makeText(this, "Title can't be void!", Toast.LENGTH_SHORT).show();
			return;
		}
		String description = ((EditText) findViewById(R.id.et_description)).getText().toString();
		wish.setTitle(title);
		wish.setDescription(description);
		if (steps.size() == 0) {
			Toast.makeText(this, "Wish must have at least one step!", Toast.LENGTH_SHORT).show();
			return;
		}
			
		wish.setSteps(steps);
		
		DAO dao = DAO.getInstance(this);
		dao.insertWish(wish);
		
		Intent intent = new Intent(this, WishesActivity.class);
		startActivity(intent);
		finish();
	}
}
