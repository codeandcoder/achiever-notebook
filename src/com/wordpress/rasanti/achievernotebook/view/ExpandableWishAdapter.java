package com.wordpress.rasanti.achievernotebook.view;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import android.app.Activity;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wordpress.rasanti.achievernotebook.R;
import com.wordpress.rasanti.achievernotebook.model.DAO;
import com.wordpress.rasanti.achievernotebook.model.Step;
import com.wordpress.rasanti.achievernotebook.model.Wish;

public class ExpandableWishAdapter extends BaseExpandableListAdapter {

	private Activity activity;
	private LayoutInflater li;
	private Resources res;
	private ArrayList<Wish> achs;

	public ExpandableWishAdapter(Activity context, ArrayList<Wish> achs) {
		this.activity = context;
		this.li = context.getLayoutInflater();
		this.res = context.getResources();
		this.achs = achs;
	}

	public int getGroupCount() {
		return achs.size();
	}

	public int getChildrenCount(int groupPosition) {
		return achs.get(groupPosition).getSteps().size();
	}

	public Object getGroup(int groupPosition) {
		return achs.get(groupPosition);
	}

	public Object getChild(int groupPosition, int childPosition) {
		return achs.get(groupPosition).getSteps().get(childPosition);
	}

	public long getGroupId(int groupPosition) {
		return achs.get(groupPosition).getId();
	}

	public long getChildId(int groupPosition, int childPosition) {
		return achs.get(groupPosition).getSteps().get(childPosition).getId();
	}

	public boolean hasStableIds() {
		return false;
	}

	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		
		GroupViewHolder vh;
        Wish w = achs.get(groupPosition);

        if (convertView == null) {
                convertView = li.inflate(R.layout.wish_item, parent, false);
                vh = new GroupViewHolder(convertView);
                convertView.setTag(vh);
        } else {
                vh = (GroupViewHolder) convertView.getTag();
        }

        vh.title.setText(w.getTitle());
        vh.description.setText(w.getDescription());
        String dates = w.getFormattedInsertDate();
        if ( w.isAccomplished() ) {
        	dates += " - " + w.getFormattedCompletionDate();
        	vh.trophy.setImageDrawable(res.getDrawable(R.drawable.trophy_mini_color));
        } else {
        	vh.trophy.setImageDrawable(res.getDrawable(R.drawable.trophy_mini));
        }
        vh.dates.setText(dates);
        return convertView;
	}

	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		
		ChildViewHolder vh;
        Step s = achs.get(groupPosition).getSteps().get(childPosition);

        if (convertView == null) {
                convertView = li.inflate(R.layout.step_item, parent, false);
                vh = new ChildViewHolder(convertView);
                convertView.setTag(vh);
        } else {
                vh = (ChildViewHolder) convertView.getTag();
        }
        
        vh.title.setText(s.getTitle());
        if ( s.isAccomplished() ) {
        	vh.accomplished.setImageDrawable(res.getDrawable(R.drawable.trophy_mini_color));
        } else {
        	vh.accomplished.setImageDrawable(res.getDrawable(R.drawable.trophy_mini));
        }
        return convertView;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	private static class GroupViewHolder {

		ImageView trophy;
		TextView title;
		TextView description;
		TextView dates;

		public GroupViewHolder(View view) {
			this.trophy = (ImageView) view.findViewById(R.id.iv_accomplished);
			this.title = (TextView) view.findViewById(R.id.tv_title);
			this.description = (TextView) view.findViewById(R.id.tv_description);
			this.dates = (TextView) view.findViewById(R.id.tv_dates);
		}
	}
	
	private static class ChildViewHolder {

		TextView title;
		ImageView accomplished;

		public ChildViewHolder(View view) {
			this.title = (TextView) view.findViewById(R.id.tv_title);
			this.accomplished = (ImageView) view.findViewById(R.id.iv_accomplished);
		}
	}
	
	public void switchAccomplishmentStep(final int wishIndex,final int stepIndex) {
		final DAO dao = DAO.getInstance(activity);
		activity.runOnUiThread(new Runnable() {
            public void run() {
                Step step = achs.get(wishIndex).getSteps().get(stepIndex);
                if ( stepIndex > 0 ) {
                	if ( achs.get(wishIndex).getSteps().get(stepIndex-1).isAccomplished() 
                			&& (achs.get(wishIndex).getSteps().size() == stepIndex+1
                					|| !achs.get(wishIndex).getSteps().get(stepIndex+1).isAccomplished() )) {
                		step.setAccomplished(!step.isAccomplished());
                		dao.updateStep(step);
                	}
                } else if ( achs.get(wishIndex).getSteps().size() == 1
                		|| !achs.get(wishIndex).getSteps().get(stepIndex+1).isAccomplished()){
                	step.setAccomplished(!step.isAccomplished());
                	dao.updateStep(step);
                }
                
                if ( step.isAccomplished()
                		&& achs.get(wishIndex).getSteps().size() == stepIndex+1 ) {
                	completeWish(wishIndex);
                }
                
                notifyDataSetChanged();
            }
        });
	}
	
	private void completeWish(int wishIndex) {
		achs.get(wishIndex).setCompletionDate(new Date());
		DAO dao = DAO.getInstance(activity);
		dao.updateWish(achs.get(wishIndex));
		achs.remove(wishIndex);
	}

}
